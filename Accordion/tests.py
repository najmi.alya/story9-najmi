from django.test import TestCase, Client, override_settings
from django.apps import apps
from django.urls import reverse, resolve
from .views import *
from django.contrib.auth.models import User

class story7UnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('tester', '', 'password')

    def test_Homepage_url_is_exist(self):
        self.client.login(username='tester', password='password')
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    # def test_app_is_exist(self):
    #     self.assertEqual(AccordionConfig.name, 'Accordion')
    #     self.assertEqual(apps.get_app_config('Accordion').name, 'Accordion')

    def test_url_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, Homepage)

    def test_template_used(self):
        self.client.login(username='tester', password='password')
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'Homepage.html')
    
    def test_check_template(self):
        self.client.login(username='tester', password='password')
        response = self.client.get('/')
        content = response.content.decode('utf8')
        self.assertIn('Activity', content)