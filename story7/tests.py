from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client, override_settings
from django.apps import apps
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from .views import *

class story7UnitTest(TestCase):
    def test_create_user(self):
        user = {
            'username' : 'ppw',
            'password1' : 'tugastory9',
            'password2' : 'tugastory9',
        }

        response = self.client.post('/signUp/', user)
        testuser = User.objects.get(username = 'ppw')
        self.assertEqual(testuser.username, 'ppw')

    def test_Library_url_login_is_exist(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_Library_url_signpup_is_exist(self):
        response = Client().get('/signUp/')
        self.assertEqual(response.status_code, 200)
    
    def test_Library_url_logout_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_url_login_using_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginUser)
    
    def test_url_signup_using_func(self):
        found = resolve('/signUp/')
        self.assertEqual(found.func, signUp)
    
    def test_url_logout_using_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logoutUser)

    def test_template_login_used(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_template_signup_used(self):
        response = Client().get('/signUp/')
        self.assertTemplateUsed(response, 'signup.html')
    
    def test_check_template_login(self):
        response = Client().get('/login/')
        content = response.content.decode('utf8')
        self.assertIn('Login', content)
    
    def test_check_template_signup(self):
        response = Client().get('/signUp/')
        content = response.content.decode('utf8')
        self.assertIn('Sign Up', content)