from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client, override_settings
from django.apps import apps
from django.urls import reverse, resolve
from .views import *
from django.contrib.auth.models import User

class story7UnitTest(TestCase):
    def test_login(self):

        # First check for the default behavior
        response = self.client.get('/Libra-ry/')
        self.assertRedirects(response, '/login/?next=/Libra-ry/')

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('tester', '', 'password')

    def test_Library_url_is_exist(self):
        self.client.login(username='tester', password='password')
        response = self.client.get('/Libra-ry/')
        self.assertEqual(response.status_code, 200)

    def test_url_using_func(self):
        found = resolve('/Libra-ry/')
        self.assertEqual(found.func, Library)

    def test_template_used(self):
        self.client.login(username='tester', password='password')
        response = self.client.get('/Libra-ry/')
        self.assertTemplateUsed(response, 'Library.html')
    
    def test_check_template(self):
        self.client.login(username='tester', password='password')
        response = self.client.get('/Libra-ry/')
        content = response.content.decode('utf8')
        self.assertIn('Library-site', content)
    
    def test_data_views(self):
        response = self.client.get('/Libra-ry/data?q=harry')
        url = self.client.get('https://www.googleapis.com/books/v1/volumes?q=harry')
        content = response.content
        content_url = url.content
        self.assertIn(content, content_url)
    
    def test_Data_url_is_exist(self):
        response = Client().get('/Libra-ry/data/?q=harry')
        self.assertEqual(response.status_code, 200)


        
    
    