from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.Library, name="Library"),
    path('data/', views.Data, name="data"),
]