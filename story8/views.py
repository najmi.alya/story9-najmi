from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import requests
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def Library(request):
    return render(request, 'Library.html', {})

def Data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    respon = requests.get(url)
    dict_data = json.loads(respon.content)
    return JsonResponse(dict_data, safe=False)